use std::error::Error;

extern crate image;
extern crate terminal_size;
use terminal_size::{Width, Height, terminal_size};

mod lib;
use lib::wm::attr::*;
use lib::wm::id::*;
use lib::canvas::*;

extern crate clap;
extern crate structopt;
use structopt::StructOpt;
use std::path::PathBuf;
use std::io;

const SW: u32 = 3840;
const SH: u32 = 2160;

#[derive(StructOpt)]
struct Command {
    #[structopt(short = "l")]
    looping: bool,
    #[structopt(flatten)]
    dim: OptDimensions,
    #[structopt(subcommand)]
    cmd: SubCmd
}

#[derive(StructOpt)]
struct OptDimensions {
    width: Option<u32>,
    height: Option<u32>,
}

struct Dimensions {
    width: u32,
    height: u32,
}

#[derive(StructOpt)]
enum SubCmd {
    #[structopt(name = "terminal")]
    Terminal,
    #[structopt(name = "picture")]
    Picture {
        #[structopt(parse(from_os_str))]
        path: PathBuf
    }
}

impl SubCmd {
    fn run(&self, d: &Dimensions) -> Result<(), Box<Error>> {
        use SubCmd::*;
        match self {
            Terminal => run_terminal(d),
            Picture{path: p} => run_picture(d, p)
        }
    }
}


fn get_attributes() -> Result<Vec<WindowAttr>, Box<Error>> {    
    let wine: Result<Vec<WindowAttr>,AttrError> = 
        Wid::query()?.into_iter()
        .map(|wid| wid.get_attributes()).collect();
    Ok(wine?)
}

fn run_terminal(d: &Dimensions) -> Result<(), Box<Error>> {
    let win = get_attributes()?;
    let size = terminal_size();
    if let Some((Width(w), Height(h))) = size {
        let c = Canvas {
                        width: w as u32, 
                        height: h as u32, 
                        screenwidth: d.width, 
                        screenheight: d.height, 
                        windows: win};
        println!("{}", Charbuf::from_canvas(c));
        Ok(())
    } else {
        Err(Box::new(io::Error::new(io::ErrorKind::Other, "cant get terminal_size()")))
    }
}

fn run_picture(d: &Dimensions, p: &PathBuf) -> Result<(), Box<Error>> {
    let win = get_attributes()?;
    let c = Canvas {
        width: d.width, 
        height: d.height, 
        screenwidth: SW, 
        screenheight: SH, 
        windows: win
    };
    let _ = image::ImageLuma8(GrayImageBuf::from(c).0).save(p);
    Ok(())
}

impl Command {
    fn run(&self) -> Result<(), Box<Error>> {
        let dim = Dimensions {
            width: self.dim.width.unwrap_or_else(|| SW) as u32,
            height: self.dim.height.unwrap_or_else(|| SH)  as u32,
        };
        if self.looping {
            loop {
                self.cmd.run(&dim);
            }    
        } else{ 
            self.cmd.run(&dim)
        }
    }
}

fn main() -> Result<(), Box<Error>> {
    let cmd: Command = Command::from_args();
    cmd.run()
}