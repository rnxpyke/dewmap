use std::process::Command;
use std::convert::TryFrom;
use std::process::Output;
use std::fmt;

use super::attr::*;
extern crate hex;

use self::errors::*;

#[derive(Debug)]
pub struct Wid([u8;4]);

impl fmt::Display for Wid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<Wid> for String {
    fn from(w: Wid) -> Self {
        hex::encode(w.0)
    }
}

impl TryFrom<String> for Wid {
    type Error = WidParseError;
    fn try_from(s: String) -> Result<Self, WidParseError> {
        Wid::try_from(s.as_str())
    }
}

impl TryFrom<&str> for Wid {
    type Error = WidParseError;
    fn try_from(s: &str) -> Result<Self, WidParseError> {
        use WidParseError::*;
        fn pop(s: Vec<u8>) -> [u8;4] {
            [s[0], s[1], s[2], s[3]]
        }
        let slice = if s.starts_with("0x") {
                let e = &s[2..];
                hex::decode(e)?
            } else 
            {
                hex::decode(s)?
            };
        if slice.len() < 4 {
            Err(ToShort)
        } else
        {
            Ok(Wid(pop(slice)))
        }
    }
}

impl From<&Wid> for String {
    fn from(w: &Wid) -> Self {
        hex::encode(w.0)
    }
}
impl Wid {
    pub fn query() -> Result<Vec<Self>, WidQueryError> {
        let out = Command::new("sh")
            .arg("-c")
            .arg("lsw")
            .output()?;
        String::from_utf8(out.stdout)?
            .lines()
            .map(|s| Wid::try_from(s).map_err(|e| WidQueryError::from(e)))
            .collect()
    }
}

impl Wid {
    pub fn get_attributes(&self) -> Result<WindowAttr, AttrError> {
        let s: String = String::from("0x") + &String::from(self);
        let out: Output = Command::new("sh")
            .arg("-c")
            .arg(String::from("wattr xywh ") + &s)
            .output()?;
        if out.status.success() 
        {
            let s = String::from_utf8(out.stdout); 
            let ve:Vec<Result<i32,AttrError>> = s.map_err(|_| AttrError::InvalidUtf8)?
                .trim_end()
                .split(' ')
                .map(|x| x.parse::<i32>()
                        .map_err(|_| AttrError::NaN))
                .collect();
            let mut v = ve.into_iter();
            let attr = WindowAttr {
                xpos   : v.next().ok_or_else(|| AttrError::NaN)??,
                ypos   : v.next().ok_or_else(|| AttrError::NaN)??,
                width  : v.next().ok_or_else(|| AttrError::NaN)??,
                height : v.next().ok_or_else(|| AttrError::NaN)??,
            };
            if v.next().is_none() {
                Ok(attr)
            } else 
            {
                Err(AttrError::ToLong)
            }
        } else 
        {
            Err(AttrError::Unsucessfull)
        }
    }
}

mod errors {
    use std::fmt;
    use std::io;
    use std::string;
    use std::error::Error;
    #[derive(Debug)]
    pub enum WidParseError{
        ToShort,
        HexError
    }

    impl fmt::Display for WidParseError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    impl Error for WidParseError {
        fn cause(&self) -> Option<&Error> {
            None
        }
    }

    impl From<hex::FromHexError> for WidParseError {
        fn from(_e: hex::FromHexError) -> Self {
            WidParseError::HexError
        }
    }



    #[derive(Debug)]
    pub enum WidQueryError {
        IO(Box<io::Error>),
        InvalidUtf8,
        ParseError
    }
    impl From<io::Error> for WidQueryError {
        fn from(e: io::Error) -> Self {
            WidQueryError::IO(Box::new(e))
        }
    }

    impl From<string::FromUtf8Error> for WidQueryError {
        fn from(_e: string::FromUtf8Error) -> Self
        {
            WidQueryError::InvalidUtf8
        }
    }

    impl From<WidParseError> for WidQueryError {
        fn from(_e: WidParseError) -> Self {
            WidQueryError::ParseError
        }
    }

    impl fmt::Display for WidQueryError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    impl Error for WidQueryError {
        fn cause(&self) -> Option<&Error> {
            None
        }
    }
}