use std::error::Error;
use std::io;
use std::fmt;

#[derive(Debug)]
pub struct WindowAttr {
    pub xpos  : i32,
    pub ypos  : i32,
    pub width : i32,
    pub height: i32
}

#[derive(Debug)]
pub enum AttrError {
    InvalidUtf8,
    NaN,
    ToLong,
    Unsucessfull,
    IO(Box<io::Error>)
}

impl fmt::Display for AttrError {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Error for AttrError{
    fn cause(&self) -> Option<&Error> {
        None
    }
}

impl From<io::Error> for AttrError {
    fn from(e: io::Error) -> Self {
        AttrError::IO(Box::new(e))
    }
}