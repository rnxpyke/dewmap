use std::cmp::{min,max};
use super::wm::attr::*;
use std::fmt;

extern crate image;
use image::GrayImage;

pub struct Canvas {
    pub width: u32,
    pub height: u32,
    pub screenwidth: u32,
    pub screenheight: u32,
    pub windows: Vec<WindowAttr>,
}

pub trait CanvasBuf: Sized {
    fn new(w: u32, h: u32) -> Self;
    fn draw_rect(&mut self, attr: &WindowAttr, num: usize);
    fn from_canvas(c: Canvas) -> Self {
        let xn = |x| ((x * c.width  as i32) as f64 / c.screenwidth  as f64 ) as i32;
        let yn = |y| ((y * c.height as i32) as f64 / c.screenheight as f64 ) as i32;
        let mut buf = Self::new(c.width, c.height);
        c.windows.iter()
            .enumerate()
            .for_each(|(num, val)| {
                //normalise window size
                let norm = WindowAttr {
                    xpos:   xn(val.xpos ),
                    ypos:   yn(val.ypos ),
                    width:  xn(val.width),
                    height: yn(val.height),
                };
                buf.draw_rect(&norm, num);
            });
        buf
    } 
}

pub struct Charbuf {
    width: usize,
    height: usize,
    content: Vec<u8>,
}

impl CanvasBuf for Charbuf {
    fn new(w: u32, h: u32) -> Self {
        let space = (w) as usize * (h-1) as usize;
        let mut v: Vec<u8> = Vec::with_capacity(space);
        v.resize_with(space, || ' ' as u8);
        for i in 1 .. (h-1) {
            v[(i*w -1) as usize] = '\n' as u8; 
        }
        Charbuf {
            width: w as usize,
            height: (h-1) as usize,
            content: v
        }
    }

    fn draw_rect(&mut self, attr: &WindowAttr, num: usize) {
        for i in max(0,attr.xpos as usize) .. min(self.width as usize -1, (attr.xpos + attr.width) as usize)  {
            for j in max(0,attr.ypos as usize) .. min(self.height, (attr.ypos + attr.height) as usize) {
                self.content[i + j * self.width] = '0' as u8 + num as u8; 
            }
        }
    }

}

impl From<Canvas> for Charbuf {
    fn from(c: Canvas) -> Self {
        Charbuf::from_canvas(c)
    }
}

impl fmt::Display for Charbuf {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        unsafe {
            write!(f,"{}", std::str::from_utf8_unchecked(self.content.as_slice()))
        }
    }
}

pub struct GrayImageBuf(pub GrayImage);

impl CanvasBuf for GrayImageBuf {
    fn new(w: u32, h: u32) -> Self {
        GrayImageBuf(GrayImage::new(w,h))
    }
    fn draw_rect(&mut self, attr: &WindowAttr, _num: usize) {
        let pixel = image::Luma{data: [255u8]};
        let (x,y,w,h) = 
            ( attr.xpos   as i32
            , attr.ypos   as i32
            , attr.height as i32
            , attr.width  as i32);
        for i in max(0,x) .. min(self.0.width() as i32, x + h) {
            for j in max(0,y) .. min(self.0.height() as i32, y + w) {
                self.0.put_pixel(i as u32,j as u32, pixel);
            }
        }
    }
}

impl From<Canvas> for GrayImageBuf {
    fn from(c: Canvas) -> Self {
        GrayImageBuf::from_canvas(c)
    }
}